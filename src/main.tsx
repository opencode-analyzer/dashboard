import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import "react-tooltip/dist/react-tooltip.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Projects from "./projectOverview/containers/Projects.tsx";
import { loader } from "./projectOverview/loaders/loader.tsx";
import { loader as projectLoader } from "./projectDetail/loaders/loader.tsx";
import Project from "./projectDetail/containers/Project.tsx";
import Error from "./sharedComponents/Error.tsx";

const router = createBrowserRouter([
    {
        path: "/",
        element: <App />,
        errorElement: <Error />,
        children: [
            {
                path: "/",
                loader: loader,
                element: <Projects />,
                errorElement: <Error />,
            },
            {
                path: "/projects/:projectId",
                // TODO: Not sure why React Router does not provide proper typing for this. Should be fixed in the future (or future version of react router)
                // eslint-disable-next-line
                loader: projectLoader as any,
                element: <Project />,
                errorElement: <Error />,
            },
        ],
    },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
    <React.StrictMode>
        <RouterProvider router={router}></RouterProvider>
    </React.StrictMode>
);
