import {
    KpiDto,
    RepositoryDto,
    ScoreCardDto,
    ToolRunDto,
    ValidateUserDto,
} from "./apiTypes";

export async function getAllRepositoryScoreCards(): Promise<ScoreCardDto[]> {
    const res = await fetch(
        `${import.meta.env.VITE_API_URL}/repository/scorecard`,
        {
            cache: "no-store",
            headers: {
                "Api-Key": `${import.meta.env.VITE_API_KEY}`,
            },
        }
    );

    if (!res.ok) {
        throw new Error("Failed to fetch all repositories");
    }

    const scorecards = (await res.json()) as ScoreCardDto[];

    // Fix that scorecard only offers first level of kpis, but that bears problems with warning calculation
    for (const scorecard of scorecards) {
        scorecard.kpis = await getKpiForRepository(
            scorecard.repository.repositoryId
        );
    }

    return scorecards;
}

export async function getRepository(id: number): Promise<RepositoryDto> {
    const res = await fetch(
        `${import.meta.env.VITE_API_URL}/repository/${id}`,
        {
            cache: "no-store",
            headers: {
                "Api-Key": `${import.meta.env.VITE_API_KEY}`,
            },
        }
    );
    if (!res.ok) {
        throw new Error("Failed to fetch repository with id " + id);
    }

    const repository = (await res.json()) as RepositoryDto;
    return repository;
}

export async function getSessionCookieForRepositoryId(
    authentication: string,
    id: number
): Promise<ValidateUserDto> {
    const body = {
        b: authentication,
    };
    const res = await fetch(
        `${import.meta.env.VITE_API_URL}/repository/${id}/validateUser`,
        {
            method: "POST",
            cache: "no-store",
            headers: {
                "Content-Type": "application/json",
                "Api-Key": `${import.meta.env.VITE_API_KEY}`,
            },
            body: JSON.stringify(body),
        }
    );
    if (!res.ok) {
        throw new Error("Failed to validate user session");
    }

    const validation = (await res.json()) as ValidateUserDto;

    console.log("Validation Response", JSON.stringify(validation));

    return validation;
}

export async function getLatestToolRuns(id: number): Promise<ToolRunDto> {
    const res = await fetch(
        `${import.meta.env.VITE_API_URL}/repository/${id}/toolrun`,
        {
            headers: {
                "Api-Key": `${import.meta.env.VITE_API_KEY}`,
            },
        }
    );

    if (!res.ok) {
        throw new Error(`Failed to fetch tool runs for repository ${id}`);
    }

    const toolRun = (await res.json()) as ToolRunDto;
    return toolRun;
}

export async function getKpiForRepository(id: number): Promise<KpiDto> {
    const res = await fetch(
        `${import.meta.env.VITE_API_URL}/repository/${id}/kpi`,
        {
            cache: "no-store",
            headers: {
                "Api-Key": `${import.meta.env.VITE_API_KEY}`,
            },
        }
    );

    if (!res.ok) {
        throw new Error(`Failed to fetch KPI for repository ${id}`);
    }

    const kpis = (await res.json()) as KpiDto;
    return kpis;
}
