import { KpiDto } from "./apiTypes";
import { getTypeForKpiName } from "./getTypeForKpiName";

export const flattenKpiTree = (root: KpiDto): KpiDto[] => {
    const result: KpiDto[] = [];
    function traverse(node: KpiDto) {
        result.push(node);

        for (const child of node.children) {
            traverse(child.kpi);
        }
    }
    traverse(root);
    return result;
};

export const flattenAndFilterKpiTreeForCritical = (root: KpiDto): KpiDto[] => {
    const flattenedKpiTree = flattenKpiTree(root);
    return flattenedKpiTree.filter(
        (kpi) =>
            kpi.hasLowScore &&
            !kpi.isEmpty &&
            getTypeForKpiName(kpi.name) !== "raw value"
    );
};

export const flattenAndFilterKpiTreeForInsufficient = (
    root: KpiDto
): KpiDto[] => {
    const flattenedKpiTree = flattenKpiTree(root);
    return flattenedKpiTree.filter(
        (kpi) =>
            kpi.amountOfMissingDataChildren >= 0.5 &&
            !kpi.isEmpty &&
            getTypeForKpiName(kpi.name) !== "raw value"
    );
};
