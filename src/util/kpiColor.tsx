export const getKpiColor = (percentage: number) => {
    if (percentage > 85) {
        return "var(--green)";
    } else if (percentage > 40) {
        return "var(--orange)";
    } else {
        return "var(--red)";
    }
};
