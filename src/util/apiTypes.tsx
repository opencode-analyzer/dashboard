export type RepositoryDto = {
    id: string;
    name: string;
    repositoryId: number;
    url: string;
};

export type ScoreCardDto = {
    repository: RepositoryDto;
    kpis: KpiDto;
};

export type KpiDto = {
    id: string;
    name: string;
    description: string;
    repositoryId: string;
    isRoot: boolean;
    value: number;
    maxValue: 100;
    amountOfMissingDataChildren: number;
    children: ChildDto[];
    displayValue?: string;
    isEmpty: boolean;
    order: number;
    hasLowScore: boolean;
};

export type ChildDto = {
    plannedWeight: number;
    actualWeight: number;
    kpi: KpiDto;
};

export type ToolRunDto = {
    isProjectMember: boolean;
    toolRun: {
        id: string;
        createdAt: string;
        repoId: number;
        tools: ToolDto[];
        languages: LanguageDto[];
    };
};

export type LanguageDto = {
    name: string;
    percentage: number;
};

export type FindingsDto = {
    message: string;
};

export type ToolDto = {
    name: string;
    description: string;
    relevantKpis: string[];
    findings: FindingsDto[];
};

export type ValidateUserDto = {};
