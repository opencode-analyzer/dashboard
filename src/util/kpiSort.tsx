import { KpiDto } from "./apiTypes";

export const kpiSort = (
    a: { kpi: KpiDto; plannedWeight: number; actualWeight: number; },
    b: { kpi: KpiDto; plannedWeight: number; actualWeight:number; }
) => a.kpi.order - b.kpi.order;
