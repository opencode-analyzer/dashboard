export const calcPercentage = (currentValue: number, maxValue: number) =>
    Math.round((currentValue / maxValue) * 100);
