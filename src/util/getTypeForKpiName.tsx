// TODO: Remove this in the future, for now we use the manual mapping of types
export function getTypeForKpiName(name: string) {
    const cveNamePattern = /^CVE-\d+-\d+$/;

    if (
        name === "Number of Commits" ||
        name === "Number of Signed Commits" ||
        name.startsWith("Vulnerability") ||
        cveNamePattern.test(name)
    ) {
        return "raw value";
    } else if (name === "Commit Signature Ratio") {
        return "ratio";
    } else if (name === "Maximal Dependency Vulnerability Score") {
        return "maximum";
    } else if (name === "Default Branch Protected") {
        return "boolean";
    } else {
        return "aggregation";
    }
}
