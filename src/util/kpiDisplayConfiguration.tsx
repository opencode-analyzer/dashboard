import {
    IconDefinition,
    faDatabase,
    faGaugeHigh,
} from "@fortawesome/free-solid-svg-icons";
import { getTypeForKpiName } from "./getTypeForKpiName";

type KpiDisplayConfiguration = {
    icon: IconDefinition;
    showWeight: boolean;
    showScoreBar: boolean;
};

export const kpiDisplayConfiguration: {
    [key: string]: KpiDisplayConfiguration;
} = {
    "raw value": {
        icon: faDatabase,
        showWeight: false,
        showScoreBar: false,
    },
    aggregation: {
        icon: faGaugeHigh,
        showWeight: true,
        showScoreBar: true,
    },
    ratio: {
        icon: faGaugeHigh,
        showWeight: true,
        showScoreBar: true,
    },
    maximum: {
        icon: faGaugeHigh,
        showWeight: true,
        showScoreBar: false,
    },
    boolean: {
        icon: faGaugeHigh,
        showWeight: true,
        showScoreBar: false,
    },
    default: {
        icon: faGaugeHigh,
        showWeight: true,
        showScoreBar: true,
    },
};

export const getKpiDisplayConfigurationForKpiName = (name: string) => {
    const type = getTypeForKpiName(name);
    const config: KpiDisplayConfiguration = kpiDisplayConfiguration[type]
        ? kpiDisplayConfiguration[type]
        : kpiDisplayConfiguration["default"];
    return config;
};
