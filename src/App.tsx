import { Link, Outlet, useNavigation } from "react-router-dom";
import styles from "./App.module.css";
import opencode from "./assets/opencode.svg";
import Loading from "./sharedComponents/Loading";

function App() {
    const navigation = useNavigation();

    return (
        <>
            <div className={styles.header}>
                <div className={styles.headerMain}>
                    <div className={styles.headerLeft}>
                        <Link to={"/"} style={{ display: "flex" }}>
                            <img
                                alt="Open CoDE Logo"
                                src={opencode}
                                width="200"
                                height="48"
                                style={{
                                    marginRight: "10px",
                                }}
                            />
                            <h3 style={{ color: "var(--text-white)" }}>
                                Analyzer
                            </h3>
                        </Link>
                    </div>
                    <div className={styles.headerRight}>
                        <a className={styles.active}>EN</a> / <a>DE</a>
                    </div>
                </div>
            </div>
            <main className={styles.main}>
                {navigation.state === "loading" ? <Loading /> : <Outlet />}
            </main>
        </>
    );
}

export default App;
