import { ScoreCardDto } from "../../util/apiTypes";
import RepositoryCard from "../components/RepositoryCard";
import styles from "./Projects.module.css";
import { useLoaderData } from "react-router-dom";

export default function Projects() {
    const { scorecards } = useLoaderData() as {
        scorecards: ScoreCardDto[];
    };

    return (
        <>
            <h1 style={{ color: "var(--text-red)" }}>Project Overview</h1>
            <p>
                OpenCoDE Analyzer is designed to provide you with comprehensive
                quality and security key performance indicators (KPI) for
                repositories of the OpenCoDe platform.
            </p>
            <div className={styles.grid}>
                {scorecards
                    .sort((a, b) =>
                        a.repository.name.localeCompare(b.repository.name)
                    )
                    .map((scoreCard: ScoreCardDto) => {
                        return (
                            <RepositoryCard
                                repository={scoreCard.repository}
                                kpis={scoreCard.kpis}
                                key={scoreCard.repository.id}
                            ></RepositoryCard>
                        );
                    })}
            </div>
        </>
    );
}
