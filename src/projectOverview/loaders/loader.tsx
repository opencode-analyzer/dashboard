import { getAllRepositoryScoreCards } from "../../util/apiService";

export async function loader() {
    const scorecards = await getAllRepositoryScoreCards();
    return { scorecards };
}
