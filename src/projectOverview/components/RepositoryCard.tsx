import styles from "./RepositoryCard.module.css";
import ScoreBar from "../../sharedComponents/ScoreBar";
import KpiList from "./KpiList";
import { Link } from "react-router-dom";
import { KpiDto, RepositoryDto } from "../../util/apiTypes";
import ScoreBarNotAvailable from "../../sharedComponents/ScoreBarNotAvailable";
import { kpiSort } from "../../util/kpiSort";
import { Warnings } from "../../projectDetail/components/Warnings";
import {
    flattenAndFilterKpiTreeForCritical,
    flattenAndFilterKpiTreeForInsufficient,
} from "../../util/flattenKpiTree";

interface RepositoryCardProps {
    repository: RepositoryDto;
    kpis: KpiDto;
}

export default function RepositoryCard(props: RepositoryCardProps) {
    const critical = flattenAndFilterKpiTreeForCritical(props.kpis);
    const insufficient = flattenAndFilterKpiTreeForInsufficient(props.kpis);
    return (
        <div className={styles.repositoryCard}>
            <h2 style={{ color: "var(--text-red)" }}>
                {props.repository.name + " "}
                <Warnings
                    id={props.repository.repositoryId.toString()}
                    kpi={props.kpis}
                    insufficient={insufficient}
                    critical={critical}
                />
            </h2>
            <div>
                {props.kpis && props.kpis.children && (
                    <>
                        {props.kpis.value === -1 ? (
                            <ScoreBarNotAvailable />
                        ) : (
                            <ScoreBar
                                currentValue={props.kpis.value}
                                maxValue={100}
                            ></ScoreBar>
                        )}
                        <KpiList
                            kpis={props.kpis.children
                                .sort(kpiSort)
                                .map(
                                    (kpi: {
                                        kpi: KpiDto;
                                        actualWeight: number;
                                        plannedWeight: number;
                                    }) => kpi.kpi
                                )}
                        ></KpiList>
                    </>
                )}
                <Link
                    className="button"
                    style={{ marginBottom: 10 }}
                    to={`projects/${props.repository.repositoryId}`}
                >
                    Details
                </Link>
            </div>
        </div>
    );
}
