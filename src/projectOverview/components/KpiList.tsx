import { KpiDto } from "../../util/apiTypes";
import styles from "./KpiList.module.css";

interface KpiListProps {
    kpis: KpiDto[];
}

export default function KpiList(props: KpiListProps) {
    return (
        <ul className={styles.kpiList}>
            {props.kpis.map((kpi) => {
                return (
                    <li key={`${kpi.name} ${kpi.value}`}>
                        {kpi.value === -1 ? (
                            <>
                                <span
                                    style={{
                                        color: "var(--text-light-gray)",
                                    }}
                                >
                                    {kpi.name}
                                </span>
                                <span
                                    style={{ color: "var(--text-light-gray)" }}
                                >
                                    N/A
                                </span>
                            </>
                        ) : (
                            <>
                                <span>{kpi.name}</span>
                                <span>{kpi.value} / 100</span>
                            </>
                        )}
                    </li>
                );
            })}
        </ul>
    );
}
