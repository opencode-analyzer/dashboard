import {
    getKpiForRepository,
    getLatestToolRuns,
    getRepository,
    getSessionCookieForRepositoryId,
} from "../../util/apiService";

import Cookies from "js-cookie";

export async function loader({
    request,
    params,
}: {
    request: Request;
    params: { projectId: string };
}) {
    const projectId = parseInt(params.projectId);
    const authentication = new URL(request.url).searchParams.get("b");
    const auth = {
        authentication: false,
        authorization: false,
    };

    if (authentication) {
        try {
            await getSessionCookieForRepositoryId(authentication, projectId);
        } catch (e) {
            console.error(e);
            console.error("Failed to fetch session cookie");
        }
    }

    const authCookie: string | undefined = Cookies.get(
        import.meta.env.VITE_AUTH_COOKIE
    );

    console.log("Cookie key", import.meta.env.VITE_AUTH_COOKIE);
    console.log("Got Cookie", authCookie);
    // check whether we already have a cookie
    if (authCookie) {
        auth.authentication = true;
    }

    const repositoryPromise = getRepository(projectId);
    const kpisPromise = getKpiForRepository(projectId);
    const toolRunsPromise = getLatestToolRuns(projectId);

    const [repository, kpis, toolRuns] = await Promise.all([
        repositoryPromise,
        kpisPromise,
        toolRunsPromise,
    ]);

    // not sure if we do it twice, but at least it works...
    if (toolRuns.isProjectMember) {
        auth.authentication = true;
        auth.authorization = true;
    }

    return { repository, kpis, toolRuns, auth };
}
