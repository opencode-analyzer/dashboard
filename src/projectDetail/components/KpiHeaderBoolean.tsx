import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./KpiHeader.module.css";
import {
    faCheck,
    faChevronDown,
    faDatabase,
    faWeightHanging,
    faXmark,
} from "@fortawesome/free-solid-svg-icons";

interface KpiHeaderBooleanProps {
    currentValue: number;
    name: string;
    type: string;
    expanded: boolean;
    weight?: number;
    amountOfMissingDataChildren?: number;
}

export default function KpiHeaderBoolean(props: KpiHeaderBooleanProps) {
    return (
        <div className={styles.header}>
            <h2
                style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBlock: 0,
                }}
            >
                <span className={styles.headerIconName}>
                    <FontAwesomeIcon icon={faDatabase} /> {props.name}
                    {props.weight && (
                        <span
                            style={{
                                color: "var(--text-light-gray)",
                                fontWeight: "600",
                            }}
                        >
                            {" ("}
                            <FontAwesomeIcon
                                icon={faWeightHanging}
                                width={15}
                                className="desktop-only"
                            ></FontAwesomeIcon>
                            {props.weight})
                        </span>
                    )}
                </span>
                <span className={styles.headerScoreChevron}>
                    <span>
                        {props.currentValue === 100 ? (
                            <FontAwesomeIcon
                                style={{ color: "var(--green)" }}
                                icon={faCheck}
                            />
                        ) : (
                            <FontAwesomeIcon
                                style={{ color: "var(--red)" }}
                                icon={faXmark}
                            />
                        )}
                    </span>{" "}
                    <FontAwesomeIcon
                        className={styles.chevron}
                        rotation={props.expanded ? 180 : undefined}
                        icon={faChevronDown}
                    ></FontAwesomeIcon>
                </span>
            </h2>
        </div>
    );
}
