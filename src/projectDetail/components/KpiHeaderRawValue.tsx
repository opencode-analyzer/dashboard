import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./KpiHeader.module.css";
import { faChevronDown, faDatabase } from "@fortawesome/free-solid-svg-icons";

interface KpiHeaderRawValueProps {
    currentValue: number;
    name: string;
    type: string;
    expanded: boolean;
    weight?: number;
    displayValue?: string;
}

export default function KpiHeaderRawValue(props: KpiHeaderRawValueProps) {
    return (
        <div className={styles.header}>
            <h2
                style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBlock: 0,
                }}
            >
                <span className={styles.headerIconName}>
                    <FontAwesomeIcon
                        icon={faDatabase}
                        className="desktop-only"
                    />{" "}
                    {props.name}
                </span>
                <span className={styles.headerScoreChevron}>
                    <span>
                        {props.displayValue
                            ? props.displayValue
                            : props.currentValue}
                    </span>{" "}
                    <FontAwesomeIcon
                        className={styles.chevron}
                        rotation={props.expanded ? 180 : undefined}
                        icon={faChevronDown}
                    ></FontAwesomeIcon>
                </span>
            </h2>
        </div>
    );
}
