import { Tooltip } from "react-tooltip";
import { KpiDto } from "../../util/apiTypes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";

export const WarningCritical = (props: {
    id?: string;
    critical: KpiDto[];
    kpi: KpiDto;
}) =>
    props.critical.length > 0 && (
        <>
            <Tooltip
                id={"tw-c-" + props.id + props.kpi.name.replace(/ /g, "")}
                anchorSelect={
                    "#tw-c-a-" + props.id + props.kpi.name.replace(/ /g, "")
                }
                place="top"
            >
                <div className="tooltip-container">
                    Warning: Critical value in{" "}
                    {props.critical.map((kpi) => kpi.name).join(", ")}
                </div>
            </Tooltip>

            <span style={{ marginRight: "5px" }}>
                <a
                    className="tooltip-anchor"
                    id={"tw-c-a-" + props.id + props.kpi.name.replace(/ /g, "")}
                >
                    <FontAwesomeIcon icon={faWarning}></FontAwesomeIcon>
                </a>
            </span>
        </>
    );
