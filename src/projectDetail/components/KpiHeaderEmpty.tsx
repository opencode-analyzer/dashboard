import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./KpiHeader.module.css";
import { faChevronDown, faXmark } from "@fortawesome/free-solid-svg-icons";
import { KpiDto } from "../../util/apiTypes";

interface KpiHeaderEmptyProps {
    kpi: KpiDto;
    expanded: boolean;
    actualWeight?: number;
    plannedWeight?: number;
}

export default function KpiHeaderEmpty(props: KpiHeaderEmptyProps) {
    return (
        <div className={styles.header}>
            <h2
                style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBlock: 0,
                }}
            >
                <span
                    className={styles.headerIconName}
                    style={{ color: "var(--text-light-gray)" }}
                >
                    <FontAwesomeIcon icon={faXmark} className="desktop-only" />{" "}
                    {props.kpi.name}
                </span>
                <span className={styles.headerScoreChevron}>
                    <FontAwesomeIcon
                        className={styles.chevron}
                        rotation={props.expanded ? 180 : undefined}
                        icon={faChevronDown}
                    ></FontAwesomeIcon>
                </span>
            </h2>
        </div>
    );
}
