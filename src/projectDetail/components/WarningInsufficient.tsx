import { Tooltip } from "react-tooltip";
import { KpiDto } from "../../util/apiTypes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";

export const WarningInsufficient = (props: {
    id?: string;
    insufficient: KpiDto[];
    kpi: KpiDto;
}) =>
    props.insufficient.length > 0 && (
        <>
            <Tooltip
                id={"tw-i-" + props.id + props.kpi.name.replace(/ /g, "")}
                anchorSelect={
                    "#tw-i-a-" + props.id + props.kpi.name.replace(/ /g, "")
                }
                place="top"
            >
                <div className="tooltip-container">
                    Warning: Insufficient data in{" "}
                    {props.insufficient.map((kpi) => kpi.name).join(", ")}
                </div>
            </Tooltip>

            <span style={{ marginRight: "5px" }}>
                <a
                    className="tooltip-anchor"
                    id={"tw-i-a-" + props.id + props.kpi.name.replace(/ /g, "")}
                >
                    <FontAwesomeIcon icon={faWarning}></FontAwesomeIcon>
                </a>
            </span>
        </>
    );
