import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./KpiHeader.module.css";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { getKpiColor } from "../../util/kpiColor";
import { calcPercentage } from "../../util/calcPercentage";
import { KpiDto } from "../../util/apiTypes";
import { getTypeForKpiName } from "../../util/getTypeForKpiName";
import {
    flattenAndFilterKpiTreeForCritical,
    flattenAndFilterKpiTreeForInsufficient,
} from "../../util/flattenKpiTree";
import { Warnings } from "./Warnings";

interface KpiHeaderRawValueProps {
    kpi: KpiDto;
    expanded: boolean;
    actualWeight?: number;
    plannedWeight?: number;
}

export default function KpiHeaderScoreChevron(props: KpiHeaderRawValueProps) {
    const perc = calcPercentage(props.kpi.value, 100);
    const Percentage = () => (
        <>
            <span style={{ color: getKpiColor(perc) }}>{props.kpi.value}</span>{" "}
            / 100
        </>
    );
    const RawValue = () => (
        <span>
            {props.kpi.displayValue ? props.kpi.displayValue : props.kpi.value}
        </span>
    );
    const NoValue = () => <span></span>;
    const HeaderValue = () => {
        switch (getTypeForKpiName(props.kpi.name)) {
            case "raw value":
                return <RawValue />;
            case "aggregation":
            case "ratio":
                return <Percentage />;
            default:
                return <NoValue />;
        }
    };

    const criticalKpis = flattenAndFilterKpiTreeForCritical(props.kpi);
    const insufficientKpis = flattenAndFilterKpiTreeForInsufficient(props.kpi);

    return (
        <>
            <span className={styles.headerScoreChevron}>
                <Warnings
                    critical={criticalKpis}
                    insufficient={insufficientKpis}
                    kpi={props.kpi}
                />
                <HeaderValue />
                <FontAwesomeIcon
                    className={styles.chevron}
                    rotation={props.expanded ? 180 : undefined}
                    icon={faChevronDown}
                ></FontAwesomeIcon>
            </span>
        </>
    );
}
