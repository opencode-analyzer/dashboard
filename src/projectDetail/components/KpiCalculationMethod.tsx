import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    IconDefinition,
    faArrowUpRightDots,
    faDivide,
    faPlus,
} from "@fortawesome/free-solid-svg-icons";

interface KpiCalculationMethodProps {
    type: string;
}

export default function KpiCalculationMethod(props: KpiCalculationMethodProps) {
    const typeMapping: {
        [key: string]: { icon: IconDefinition; text: string };
    } = {
        maximum: {
            icon: faArrowUpRightDots,
            text: "Maximum",
        },
        aggregation: {
            icon: faPlus,
            text: "Aggregation",
        },
        ratio: {
            icon: faDivide,
            text: "Ratio",
        },
    };

    const type = typeMapping[props.type];

    if (!type) {
        throw new Error("Type not found");
    }

    return (
        <div className="desktop-only-margin-right">
            <FontAwesomeIcon icon={type.icon}></FontAwesomeIcon>
            <span className="desktop-only"> {type.text}</span>
        </div>
    );
}
