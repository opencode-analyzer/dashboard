import styles from "./KpiHeader.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faArrowLeft,
    faWeightHanging,
} from "@fortawesome/free-solid-svg-icons";
import { KpiDto } from "../../util/apiTypes";
import { getKpiDisplayConfigurationForKpiName } from "../../util/kpiDisplayConfiguration";

interface KpiNameWeightProps {
    kpi: KpiDto;
    actualWeight?: number;
    plannedWeight?: number;
}

export default function KpiNameWeight(props: KpiNameWeightProps) {
    const kpiDisplayConfiguration = getKpiDisplayConfigurationForKpiName(
        props.kpi.name
    );

    return (
        <span className={styles.headerIconName}>
            <FontAwesomeIcon
                icon={kpiDisplayConfiguration.icon}
                className="desktop-only"
            />{" "}
            {props.kpi.name}
            {kpiDisplayConfiguration.showWeight &&
                props.actualWeight &&
                props.plannedWeight && (
                    <span
                        style={{
                            color: "var(--text-light-gray)",
                            fontWeight: "600",
                        }}
                    >
                        {" ("}
                        <FontAwesomeIcon
                            icon={faWeightHanging}
                            width={15}
                            className="desktop-only"
                        ></FontAwesomeIcon>
                        {props.actualWeight !== props.plannedWeight ? (
                            <>
                                {(
                                    Math.round(props.actualWeight * 100) / 100
                                ).toFixed(2)}
                                <FontAwesomeIcon icon={faArrowLeft} />
                                {(
                                    Math.round(props.plannedWeight * 100) / 100
                                ).toFixed(2)}
                            </>
                        ) : (
                            (
                                Math.round(props.actualWeight * 100) / 100
                            ).toFixed(2)
                        )}
                        )
                    </span>
                )}
        </span>
    );
}
