import Card from "../../sharedComponents/Card";
import TwoCards from "../../sharedComponents/TwoCards";
import { LanguageDto, RepositoryDto } from "../../util/apiTypes";
import styles from "./ProjectInformation.module.css";
import { Link } from "react-router-dom";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";
import { Colors } from "chart.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWarning } from "@fortawesome/free-solid-svg-icons";
import CardWarning from "../../sharedComponents/CardWarning";

interface ProjectInformationProps {
    repository: RepositoryDto;
    languages?: LanguageDto[];
    warningCritical: string[];
    warningInsufficient: string[];
}

ChartJS.register(Colors);
ChartJS.register(ArcElement, Tooltip, Legend);

const InfoList = (props: ProjectInformationProps) => (
    <ul className={styles.list}>
        <li>
            <span>ID</span>
            <span>{props.repository.id}</span>
        </li>
        <li>
            <span>Name</span>
            <span>{props.repository.name}</span>
        </li>
        <li>
            <span>Platform</span>
            <span>Open CoDE GitLab</span>
        </li>
        <li>
            <span>Repository ID on platform</span>
            <span>{props.repository.repositoryId}</span>
        </li>
        <li>
            <span>OpenCoDE GitLab</span>
            <span>
                <Link to={props.repository.url}>Repository</Link>
            </span>
        </li>
    </ul>
);

const Warnings = (props: ProjectInformationProps) =>
    (props.warningCritical.length > 0 ||
        props.warningInsufficient.length > 0) && (
        <>
            {props.warningInsufficient.length > 0 && (
                <CardWarning>
                    <div style={{ fontWeight: 800 }}>
                        <FontAwesomeIcon
                            icon={faWarning}
                            style={{ marginRight: 10, color: "var(--primary)" }}
                        />
                        Be aware that not all KPIs were calculated and that the
                        project score might not be representive (
                        {props.warningInsufficient.join(", ")})
                    </div>
                </CardWarning>
            )}
            {props.warningCritical.length > 0 && (
                <CardWarning>
                    <div style={{ fontWeight: 800 }}>
                        <FontAwesomeIcon
                            icon={faWarning}
                            style={{ marginRight: 10, color: "var(--primary)" }}
                        />
                        Be aware that there are KPIs with critically low values
                        ({props.warningCritical.join(", ")})
                    </div>
                </CardWarning>
            )}
        </>
    );

export default function ProjectInformation(props: ProjectInformationProps) {
    if (props.languages && props.languages.length > 0) {
        const data = {
            labels: props.languages.map((language) => language.name),
            datasets: [
                {
                    label: "Language percentage in project",
                    data: props.languages.map(
                        (language) => language.percentage
                    ),
                    borderWidth: 2,
                },
            ],
        };

        return (
            <TwoCards
                childrenLeftFirst={<InfoList {...props} />}
                childrenLeftSecond={<Warnings {...props} />}
                childrenRight={<Pie data={data} />}
            />
        );
    }

    return (
        <Card>
            <InfoList {...props} />
        </Card>
    );
}
