import styles from "./KpiHeader.module.css";
import { getKpiColor } from "../../util/kpiColor";

interface KpiScoreBarProps {
    percentage: number;
}

export default function KpiScoreBar(props: KpiScoreBarProps) {
    return (
        <div className={styles.scoreBar}>
            <div
                style={{
                    width: props.percentage + "%",
                    backgroundColor: getKpiColor(props.percentage),
                }}
            ></div>
        </div>
    );
}
