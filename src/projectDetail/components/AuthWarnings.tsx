import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CardWarning from "../../sharedComponents/CardWarning";
import { faWarning } from "@fortawesome/free-solid-svg-icons";

export const AuthWarnings = (props: {
    auth: {
        authentication: boolean;
        authorization: boolean;
    };
}) => (
    <>
        {!props.auth.authentication && !props.auth.authorization && (
            <CardWarning>
                <div style={{ fontWeight: 800 }}>
                    <FontAwesomeIcon
                        icon={faWarning}
                        style={{ marginRight: 10, color: "var(--primary)" }}
                    />
                    Be aware that tool findings are only displayed when logged
                    in and at least developer permissions are granted to you via
                    GitLab
                </div>
            </CardWarning>
        )}
        {props.auth.authentication && !props.auth.authorization && (
            <CardWarning>
                <div style={{ fontWeight: 800 }}>
                    <FontAwesomeIcon
                        icon={faWarning}
                        style={{ marginRight: 10, color: "var(--primary)" }}
                    />
                    Be aware that tool findings are only displayed when at least
                    developer permissions are granted to you via GitLab
                </div>
            </CardWarning>
        )}
    </>
);
