import styles from "./KpiHeader.module.css";
import KpiHeaderScoreChevron from "./KpiHeaderScoreChevron";
import KpiScoreBar from "./KpiScoreBar";
import { calcPercentage } from "../../util/calcPercentage";
import KpiNameWeight from "./KpiNameWeight";
import { KpiDto } from "../../util/apiTypes";
import { getKpiDisplayConfigurationForKpiName } from "../../util/kpiDisplayConfiguration";

interface KpiHeaderProps {
    kpi: KpiDto;
    expanded: boolean;
    actualWeight?: number;
    plannedWeight?: number;
}

export default function KpiHeader(props: KpiHeaderProps) {
    const percentage = calcPercentage(props.kpi.value, 100);
    const kpiDisplayConfiguration = getKpiDisplayConfigurationForKpiName(
        props.kpi.name
    );

    return (
        <div className={styles.header}>
            <h2
                style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBlock: 0,
                }}
            >
                <KpiNameWeight {...props} />
                <KpiHeaderScoreChevron {...props} />
            </h2>
            {kpiDisplayConfiguration.showScoreBar && (
                <KpiScoreBar percentage={percentage} />
            )}
        </div>
    );
}
