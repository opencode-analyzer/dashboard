import { KpiDto } from "../../util/apiTypes";
import { WarningCritical } from "./WarningCritical";
import { WarningInsufficient } from "./WarningInsufficient";

export const Warnings = (props: {
    id?: string;
    critical: KpiDto[];
    insufficient: KpiDto[];
    kpi: KpiDto;
}) => (
    <>
        <WarningCritical {...props} />
        <WarningInsufficient {...props} />
    </>
);
