import Card from "../../sharedComponents/Card";
import { Accordion, AccordionItem } from "@szhsin/react-accordion";
import styles from "./KpiCard.module.css";
import { getTypeForKpiName } from "../../util/getTypeForKpiName";
import { KpiDto } from "../../util/apiTypes";
import KpiCalculationMethod from "./KpiCalculationMethod";
import { kpiSort } from "../../util/kpiSort";
import KpiHeaderEmpty from "./KpiHeaderEmpty";
import KpiHeader from "./KpiHeader";

interface KpiCardProps {
    kpi: KpiDto;
    plannedWeight?: number;
    actualWeight?: number;
}

export default function KpiCard(props: KpiCardProps) {
    return (
        <Accordion transition transitionTimeout={250}>
            <Card>
                <AccordionItem
                    initialEntered={props.kpi.isRoot}
                    buttonProps={{ className: styles.header }}
                    contentProps={{ className: styles.content }}
                    header={({ state }) => {
                        if (props.kpi.isEmpty) {
                            return (
                                <KpiHeaderEmpty
                                    expanded={state.isEnter}
                                    {...props}
                                />
                            );
                        }
                        return (
                            <KpiHeader expanded={state.isEnter} {...props} />
                        );
                    }}
                >
                    <h5>Description</h5>
                    <p>{props.kpi.description}</p>

                    {props.kpi.children && props.kpi.children.length > 0 && (
                        <>
                            <h5>Calculation</h5>
                            {props.kpi.children.sort(kpiSort).map((child) => (
                                <div
                                    key={child.kpi.name + child.kpi.value}
                                    style={{
                                        display: "flex",
                                        flexDirection: "column",
                                        marginBottom: 20,
                                    }}
                                >
                                    <div
                                        style={{
                                            display: "flex",
                                            flex: 1,
                                            flexDirection: "row",
                                            alignItems: "flex-start",
                                        }}
                                    >
                                        <KpiCalculationMethod
                                            type={getTypeForKpiName(
                                                props.kpi.name
                                            )}
                                        />
                                        <div style={{ flex: 1 }}>
                                            <KpiCard
                                                kpi={child.kpi}
                                                actualWeight={
                                                    child.actualWeight
                                                }
                                                plannedWeight={
                                                    child.plannedWeight
                                                }
                                            ></KpiCard>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </>
                    )}
                </AccordionItem>
            </Card>
        </Accordion>
    );
}
