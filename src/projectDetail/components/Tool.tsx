import Card from "../../sharedComponents/Card";
import { Accordion, AccordionItem } from "@szhsin/react-accordion";
import styles from "./Tool.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { ToolDto } from "../../util/apiTypes";

interface ToolProps {
    tool: ToolDto;
    createdAt: string;
}

const noFindingsErrorMessages: { [id: string]: string } = {
    "Gitlab API": "Findings are displayed directly in the KPI hierarchy.",
};

export default function Tool(props: ToolProps) {
    return (
        <Accordion transition transitionTimeout={250}>
            <Card>
                <AccordionItem
                    buttonProps={{ className: styles.header }}
                    contentProps={{ className: styles.content }}
                    header={({ state }) => (
                        <div className={styles.header}>
                            <h2
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    marginBlock: 0,
                                }}
                            >
                                <span>{props.tool.name}</span>
                                <span>
                                    <FontAwesomeIcon
                                        className={styles.chevron}
                                        rotation={
                                            state.isEnter ? 180 : undefined
                                        }
                                        icon={faChevronDown}
                                    ></FontAwesomeIcon>
                                </span>
                            </h2>
                        </div>
                    )}
                >
                    <h5>Description</h5>
                    <p>{props.tool.description}</p>
                    <h5>Latest Run</h5>
                    <p>
                        {new Date(props.createdAt).toLocaleDateString("DE") +
                            " - " +
                            new Date(props.createdAt).toLocaleTimeString("DE")}
                    </p>
                    {props.tool.relevantKpis.length > 0 && (
                        <>
                            <h5>Relevant KPIs</h5>
                            <ul>
                                {props.tool.relevantKpis.map((kpi) => (
                                    <li key={"rel-kpi-" + kpi}>{kpi}</li>
                                ))}
                            </ul>
                        </>
                    )}
                    {props.tool.findings.length > 0 ? (
                        <>
                            <h5>Findings</h5>
                            <ul>
                                {props.tool.findings.map((finding) => (
                                    <li
                                        key={"tool-" + finding.message}
                                        style={{ wordBreak: "break-word" }}
                                    >
                                        {finding.message}
                                    </li>
                                ))}
                            </ul>
                        </>
                    ) : (
                        noFindingsErrorMessages[props.tool.name] && (
                            <>
                                <h5>Findings</h5>
                                {noFindingsErrorMessages[props.tool.name]}
                            </>
                        )
                    )}
                </AccordionItem>
            </Card>
        </Accordion>
    );
}
