import { useLoaderData } from "react-router-dom";
import KpiCard from "../components/KpiCard";
import ProjectInformation from "../components/ProjectInformation";
import Tool from "../components/Tool";
import {
    flattenAndFilterKpiTreeForCritical,
    flattenAndFilterKpiTreeForInsufficient,
} from "../../util/flattenKpiTree";
import {
    KpiDto,
    RepositoryDto,
    ToolDto,
    ToolRunDto,
} from "../../util/apiTypes";
import { AuthWarnings } from "../components/AuthWarnings";

export default function Project() {
    const { repository, kpis, toolRuns, auth } = useLoaderData() as {
        repository: RepositoryDto;
        kpis: KpiDto;
        toolRuns: ToolRunDto;
        auth: {
            authentication: boolean;
            authorization: boolean;
        };
    };

    const critical = flattenAndFilterKpiTreeForCritical(kpis).map(
        (kpi) => kpi.name
    );
    const insufficient = flattenAndFilterKpiTreeForInsufficient(kpis).map(
        (kpi) => kpi.name
    );

    return (
        <>
            <h1 style={{ color: "var(--text-red)" }}>Project Analysis</h1>
            <h2>Information and Language Distribution</h2>
            <ProjectInformation
                repository={repository}
                languages={toolRuns.toolRun.languages}
                warningCritical={critical}
                warningInsufficient={insufficient}
            />
            <h2>Calculated KPIs</h2>
            <KpiCard kpi={kpis}></KpiCard>
            <h2>Executed Analysis Tools and their Results</h2>
            <AuthWarnings auth={auth} />
            {toolRuns.toolRun.tools.length > 0 && (
                <>
                    {toolRuns.toolRun.tools.map((tool: ToolDto) => (
                        <Tool
                            key={"tool-run-" + tool.name}
                            tool={tool}
                            createdAt={toolRuns.toolRun.createdAt}
                        ></Tool>
                    ))}
                </>
            )}
        </>
    );
}
