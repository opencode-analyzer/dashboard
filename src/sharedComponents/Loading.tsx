import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Loading() {
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                width: "100%",
                paddingTop: "25px",
            }}
        >
            <h3 style={{ textAlign: "center" }}>Loading ...</h3>
            <FontAwesomeIcon icon={faSpinner} size="2x" className="fa-spin" />
        </div>
    );
}
