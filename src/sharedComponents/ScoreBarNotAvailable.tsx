import styles from "./ScoreBar.module.css";

export default function ScoreBarNotAvailable() {
    return (
        <>
            <h2>
                <span style={{ color: "var(--text-light-gray)" }}>N/A</span>
            </h2>
            <div className={styles.scoreBar}>
                <div
                    style={{
                        width: "0",
                        backgroundColor: "var(--text-light-gray)",
                    }}
                ></div>
            </div>
        </>
    );
}
