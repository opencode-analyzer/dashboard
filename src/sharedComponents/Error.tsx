import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Error() {
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                width: "100%",
                paddingTop: "25px",
            }}
        >
            <h1 style={{ textAlign: "center" }}>Something went wrong ...</h1>
            <FontAwesomeIcon icon={faTriangleExclamation} size="2x" />
        </div>
    );
}
