import styles from "./Card.module.css";
import { ReactNode } from "react";

interface TwoCardsProps {
    childrenLeftFirst?: ReactNode;
    childrenLeftSecond?: ReactNode;
    childrenRight?: ReactNode;
}

export default function TwoCards(props: TwoCardsProps) {
    return (
        <div className={styles.cardGrid}>
            <div>
                <div className={styles.card}>{props.childrenLeftFirst}</div>
                {props.childrenLeftSecond}
            </div>
            <div className={styles.card}>{props.childrenRight}</div>
        </div>
    );
}
