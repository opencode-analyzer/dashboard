import styles from "./ScoreBar.module.css";

interface ScoreBarProps {
    currentValue: number;
    maxValue: number;
}

export default function ScoreBar(props: ScoreBarProps) {
    const percentage = Math.round((props.currentValue / props.maxValue) * 100);

    const getColor = (percentage: number) => {
        if (percentage > 85) {
            return "var(--green)";
        } else if (percentage > 40) {
            return "var(--orange)";
        } else if (props.currentValue == -1) {
            return "var(--text-light-gray)";
        } else {
            return "var(--red)";
        }
    };

    return (
        <>
            <h2>
                <span style={{ color: getColor(percentage) }}>
                    {props.currentValue}
                </span>{" "}
                / {props.maxValue}
            </h2>
            <div className={styles.scoreBar}>
                <div
                    style={{
                        width: percentage + "%",
                        backgroundColor: getColor(percentage),
                    }}
                ></div>
            </div>
        </>
    );
}
