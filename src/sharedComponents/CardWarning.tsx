import styles from "./Card.module.css";
import { ReactNode } from "react";

interface CardWarningProps {
    children?: ReactNode;
}

export default function CardWarning(props: CardWarningProps) {
    return <div className={styles.cardWarning}>{props.children}</div>;
}
