import styles from "./Card.module.css";
import { ReactNode } from "react";

interface CardProps {
    children?: ReactNode;
}

export default function Card(props: CardProps) {
    return <div className={styles.card}>{props.children}</div>;
}
