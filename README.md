# OpenCoDE Analyzer Dashboard

## About The Project

The OpenCoDE Analyzer calculates key performance indicators (KPIs) for repositories stored on the OpenCoDE Platform. KPIs are representing the current health status of the project with regards to security, quality, etc. This repository is the frontend dashboard component of the project and it visualizes each hierarchical KPI tree for each repository.

## Getting Started

### Prerequisites

-   npm

    Install node as the service uses npm as a package manager. _NOTE:_ This installation command requires [brew](https://brew.sh/) and only runs on Mac.

    ```sh
    $ brew install node
    ```

-   Open CoDE Analyzer Data Provider

    Please refer to the [Data Provider](https://gitlab.opencode.de/opencode-analyzer/data-provider) and ensure that it is properly setup and running before you continue to run the frontend component. The frontend queries the KPI information from the data provider component.

### Installation

1. Clone the repo
    ```sh
    $ git clone https://gitlab.opencode.de/opencode-analyzer/dashboard
    ```
2. Install NPM packages
    ```sh
    $ npm install
    ```

### Configuration

The frontend requires an environment variable to be set to connect to the data provider. It can either be set by `mv .env.example .env` and changing the content or by setting the environment variables directly `export VITE_API_URL=http://localhost:8080/api` and `export VITE_API_KEY=XXX`. The default/example configuration works in a local environment.

## Usage

### Development

For development, the frontend can be started with the following command:

```bash
# Start frontend in watch mode (automatically reload if files change)
$ npm run dev
```

### Production

For production, the frontend can be build with the following command:

```bash
# Build service in production mode
$ npm run build
```

## Deployment

The frontend can be deployed by using Kubernetes. The manifests can be found in the `kubernetes` directory. The overall deployment consists of the following components:

-   Deployment: A deployment with 1 replica of the pod running the container specified by the `Dockerfile`
-   ConfigMap: A configuration map handling the configuration of the URL to the data provider
-   Service: A service that maps port 3000 from the pod to port 80 and exposes the frontend to an ingress (ingress managed by externals, not part of the deployment specifications)

The frontend can also be deployed by directly building and starting the Docker container. Please refer to the official Docker documentation and see `Dockerfile`.

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

## Contact

-   Benedict Wohlers - benedict.wohlers@iem.fraunhofer.de
-   Jan-Niclas Strüwer - jan-niclas.struewer@iem.fraunhofer.de
-   Tobias Petrasch - tobias@petrasch.io
